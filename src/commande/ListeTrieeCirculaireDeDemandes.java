package commande;

import outils.Demande;
import outils.Sens;

/**
 * Une IListeCirculaire represente une liste circulaire triee.
 * @author Benjamin
 */
public class ListeTrieeCirculaireDeDemandes implements IListeTrieeCirculaire<Demande>{


	/**
	 * Nombre de trie.
	 */
    private int nbTrie;
    
    /**
     * Tableau de demandes de mont�e.
     */
    private boolean demandesMonte[];
    
    /**
     * Tableau de demandes de descente
     */
    private boolean demandesDescent[];

    /**
     * Constructeur
     * @param i int nombre de trie
     */
    public ListeTrieeCirculaireDeDemandes(int i) {
        this.nbTrie = i ;
        this.demandesMonte=new boolean[i];
        this.demandesDescent=new boolean[i];
    }

    /**
     * Renvoie la taille des tableaux de demandes, c'est � dire le nombre d'indexes qui ont la valeur true.
     * @return int la taille des tableaux de demandes.
     */
	public int taille() {
		// TODO Auto-generated method stub
		int i;
		int res=0;
		for(i=0;i<this.nbTrie;i++){
			if(this.demandesMonte[i]==true){
				res++;
			}
			if(this.demandesDescent[i]==true){
				res++;
			}
		}

		return res;
	}

	/**
	 * Contr�le si les tableaux de demandes sont vide.
	 * @return boolean vrai si les talbeaux de demandes sont vide.
	 */
	@Override
	public boolean estVide() {
		// TODO Auto-generated method stub
		if(this.taille()==0)
			return true;
		else
			return false;
	}

	/**
	 * Vide les tableaux de demandes, c'est-�-dire donne la valeur false � tous les indexes des tableaux.
	 */
	@Override
	public void vider() {
		// TODO Auto-generated method stub
		int i;
		for(i=0;i<this.nbTrie;i++){
			this.demandesMonte[i]=false;
			this.demandesDescent[i]=false;
		}
	}

	@Override
	public boolean contient(Demande e) {
		// TODO Auto-generated method stub
		int etage=(e.etage());
		if(e.enMontee()){
			if(demandesMonte[etage]){
				return true;
			}
		}
		else if(e.enDescente()){
			if(demandesMonte[etage-1]){
				return true;
			}
		}
		return false;
	}

	/**
	 * Insere la demande e dans le bon tableau.
	 * @param e E Objet � inserer.
	 * @throws IllegalArgumentException Excepetion dans le cas o� le sens est ind�fini.
	 */
	@Override
	public void inserer(Demande e) {
		// TODO Auto-generated method stub
		int etage=e.etage();
		if(e.enMontee() && etage>=0 && etage<nbTrie-1){
			this.demandesMonte[etage]=true;
		}
		else if(e.enDescente() && etage>0 && etage<this.nbTrie){
			this.demandesDescent[etage-1]=true;
		}
		else{
			throw new IllegalArgumentException();
		}
	}

	/**
	 * Supprime la demande e du bon tableau.
	 * @param e E Objet � inserer.
	 * @throws IllegalArgumentException Excepetion dans le cas o� le sens est ind�fini.
	 */
	@Override
	public void supprimer(Demande e) {
		// TODO Auto-generated method stub
		int etage=e.etage();
		if(e.enMontee() && this.demandesMonte[etage]){
			this.demandesMonte[etage]=false;
		}
		else if(e.enDescente() && this.demandesDescent[etage-1]){
			this.demandesDescent[etage-1]=false;
		}
		else{
			throw new IllegalArgumentException();
		}
	}

	/**
	 * Renvoie la demande suivant l'argument e.
	 * @param courant E Objet � inserer.
	 * @return E La demande suivante.
	 */
	@Override
	public Demande suivantDe(Demande courant) {
		// TODO Auto-generated method stub
		int etage=courant.etage();
		int i;
		if(courant.enMontee()){
			for(i=etage;i<this.nbTrie;i++){
				if(this.demandesMonte[i]){
					return (new Demande(i,Sens.MONTEE));
				}
			}
			for(i=this.nbTrie-1;i>=0;i--){
				if(this.demandesDescent[i]){
					return (new Demande(i+1,Sens.DESCENTE));
				}
			}
			for(i=0;i<=etage-1;i++){
				if(this.demandesMonte[i]){
					return (new Demande(i,Sens.MONTEE));
				}
			}
		}
		else //if(courant.enDescente())
			{
			for(i=etage-1;i>=0;i--){
				if(this.demandesDescent[i]){
					return (new Demande(i+1,Sens.DESCENTE));
				}
			}
			for(i=0;i<this.nbTrie;i++){
			if(this.demandesMonte[i]){
					return (new Demande(i,Sens.MONTEE));
				}	
			}
			for(i=this.nbTrie-1;i>=etage;i--){
				if(this.demandesDescent[i]){
					return (new Demande(i+1,Sens.DESCENTE));
				}
			}	
		}
		return null;
	}
	
	/**
	 * @return String toString de la liste de trie circulaire.
	 */
	@Override
	public String toString() {
		int i;
		String res="[";
		for(i=0;i<this.nbTrie;i++){
			if(demandesMonte[i]){
				if(res=="["){
					res=res+i+"^";
				}
				else{
					res=res+","+i+"^";
				}
			}
		}
		for(i=this.nbTrie-1;i>=0;i--){
			if(demandesDescent[i]){
				if(res=="["){
					res=res+(i+1)+"v";
				}
				else{
					res=res+","+(i+1)+"v";
				}
			}
		}
		res=res+"]";
		return res;
	}
}
