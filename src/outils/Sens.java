package outils;

/**
 * Enum listant les différentes direction dans lequel peut se mouvoir l'ascenceur.
 * @author Benjamin
 */
public enum Sens {

    MONTEE("^"),
    DESCENTE("v"),
    INDEFINI("-");

	/**
	 * Sens de l'ascenseur.
	 */
    private String sens;

    /**
     * Le sens dans lequel l'ascenseur se dirige.
     * @param sens
     */
    Sens(String sens) {
        this.sens = sens;
    }
    /**
     * Retourne la chaîne de caractères associés à la valeur sélectionnée
     * @return Une chaîne de caractère
     */
    public String toString() {
        return sens;
    }

}
