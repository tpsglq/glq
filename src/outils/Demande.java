package outils;

/**
 * Demande pour l'ascenceur.
 * @author Youssef
 */
public class Demande {

	/**
	 * Etage de la demande.
	 */
	private int numeroEtage;
	
	/**
	 * Sens de la demande.
	 */
	private Sens sens;
	
	/**
	 * Constructeur sans argument.
	 */
	public Demande() {
		this.numeroEtage = 0;
		this.sens = Sens.INDEFINI;
	}

	/**
	 * Constructeur 
	 * @param numeroEtage int num�ro de l'�tage
	 * @param sens Sens sens de l'�tage
	 */
	public Demande(int numeroEtage, Sens sens) {
		super();
		this.numeroEtage = numeroEtage;
		this.sens = sens;
	}
	
	/**
	 * Renvoie le num�ro de l'�tage de la demande.
	 * @return numeroEtage l'�tage demand�.
	 */
	public int etage()
	{
		return this.numeroEtage;
	}
	
	/**
	 * Contr�le si le sens de la demande est ind�fini.
	 * @return boolean vrai si le sens de la demande est ind�fini.
	 */
	public boolean estIndefini()
	{
		if(sens.equals(Sens.INDEFINI))
		return true;
		else return false;
	}
	
	/**
	 * Contr�le si le sens de la demande est en mont�e.
	 * @return boolean vrai si le sens de la demande est en mont�e.
	 */
	public boolean enMontee()
	{
		if(sens.equals(Sens.MONTEE))
			return true;
		else return false;
	}
	
	/**
	 * Contr�le si le sens de la demande est en descente.
	 * @return boolean vrai si le sens de la demande est en descente.
	 */
	public boolean enDescente()
	{
		if(sens.equals(Sens.DESCENTE))
			return true;
		else return false;
	}
	
	/**
	 * Passe d'un etage en descente ou en monter. Exception lev� si l'ascenseur ne monte pas ni ne descend.
	 * @throws ExceptionCabineArretee Cabine arret�.
	 */
	public void passeEtageSuivant() throws ExceptionCabineArretee
	{
		if( sens.equals(Sens.MONTEE))
		{
			this.numeroEtage++;
		} else if (sens.equals(Sens.DESCENTE)) {
			this.numeroEtage--;
		}
		else
			throw  new ExceptionCabineArretee("Cabine arret�");
	}
	
	/**
	 * Change le sens de la demande.
	 * @param s Sens
	 */
	public void changeSens(Sens s)
	{
		this.sens = s;
	}

	/**
	 * @return String toString de la demande.
	 */
	@Override
	public String toString() {
		return  numeroEtage +  sens.toString() ;
	}

	/**
	 * @param obj Object objet � comparer
	 * @return boolean vrai si l'objet courrant est �gale � l'objet pass� en argument.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Demande other = (Demande) obj;
		if (numeroEtage != other.numeroEtage)
			return false;
		if (sens == null) {
			if (other.sens != null)
				return false;
		} else if (!sens.equals(other.sens))
			return false;
		return true;
	}
	
	/**
	 * Renvoie le sens de la demande.
	 * @return sens le sens de la demande.
	 */
	public Sens sens()
	{
		return this.sens;
	}
}
