package outils;

/**
 * Excepetion de cabine arret�e.
 * @author Benjamin
 */
public class ExceptionCabineArretee extends Exception {

    /**
	 * Generated UID
	 */
	private static final long serialVersionUID = -6113222087863337510L;

	/**
	 * Exception d'arret de la cabine d'ascenceur.
	 * @param message String message de l'exception.
	 */
	public ExceptionCabineArretee(String message)
    {
        super(message);
    }
}
